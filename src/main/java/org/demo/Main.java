package org.demo;

import org.demo.service.AccountRepository;
import org.demo.service.AccountService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            AccountService as = ctx.getBean(AccountService.class);
            System.out.println(as.getAccountRepository().equals(ctx.getBean("accountRepository")));
            Arrays.stream(ctx.getBeanDefinitionNames()).forEach(System.out::println);
        };
    }

    @Bean
    public AccountService accountService(AccountRepository accountRepository) {
        return new AccountService(accountRepository);
    }
}