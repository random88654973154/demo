package ru.tictactoe.service;

import java.util.Scanner;

public class TicTacToeInputProcessor {

    private final Scanner input;

    public TicTacToeInputProcessor() {
        this.input = new Scanner(System.in);
    }

    public int getInt() {
        return input.nextInt();
    }
}
