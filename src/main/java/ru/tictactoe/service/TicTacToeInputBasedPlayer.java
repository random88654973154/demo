package ru.tictactoe.service;

import ru.tictactoe.model.TicTacToeModel;
import ru.tictactoe.model.TicTacToeSymbol;

public class TicTacToeInputBasedPlayer implements TicTacToePlayer {

    private final TicTacToeSymbol s;
    private TicTacToeInputProcessor input;

    public TicTacToeInputBasedPlayer(TicTacToeSymbol s) {
        this.s = s;
        this.input = new TicTacToeInputProcessor();
    }

    @Override
    public void doMove(TicTacToeModel field) {
        System.out.println("Player" + s + " Input position: ");
        while (!field.set(input.getInt(), input.getInt(), s)) {
            System.out.println("Player" + s + " Invalid position, try again: ");
        }
    }

    public void setInput(TicTacToeInputProcessor input) {
        this.input = input;
    }
}
