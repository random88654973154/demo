package ru.tictactoe.service;

import ru.tictactoe.model.TicTacToeModel;
import ru.tictactoe.model.TicTacToeSymbol;

import java.util.Random;

public class TicTacToeAIPlayer implements TicTacToePlayer {

    private final TicTacToeSymbol s;
    private Random rnd = new Random();

    public TicTacToeAIPlayer(TicTacToeSymbol s) {
        this.s = s;
    }

    @Override
    public void doMove(TicTacToeModel field) {
        System.out.println("Player" + s + " AI Move ");
        while (!field.set(rnd.nextInt(3) + 1, rnd.nextInt(3) + 1, s)) {
            System.out.println("Player" + s + " Stupid AI, trying again");
        }
    }

    public void setRnd(Random rnd) {
        this.rnd = rnd;
    }
}
