package ru.tictactoe.service;

import ru.tictactoe.model.TicTacToeModel;

public interface TicTacToePlayer {
    void doMove(TicTacToeModel model);
}
