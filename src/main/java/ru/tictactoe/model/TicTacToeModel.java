package ru.tictactoe.model;

public interface TicTacToeModel {
    boolean set(int i, int j, TicTacToeSymbol symbol);

    String get();

    TicTacToeSymbol winner();
}
