package ru.tictactoe.model;

public class TicTacToeSimpleModel implements TicTacToeModel {

    private final TicTacToeSymbol[] arr = new TicTacToeSymbol[9];

    @Override
    public boolean set(int i, int j, TicTacToeSymbol symbol) {
        int pos = 3 * --i + --j;

        if (pos < 0 || pos > arr.length || arr[pos] != null || winner() != null) {
            return false;
        } else {
            arr[pos] = symbol;
            return true;
        }
    }

    @Override
    public String get() {
        return toString();
    }

    @Override
    public TicTacToeSymbol winner() {
        if (arr[0] != null && arr[0] == arr[1] && arr[1] == arr[2]) {
            return arr[0];
        } else if (arr[3] != null && arr[3] == arr[4] && arr[4] == arr[5]) {
            return arr[3];
        } else if (arr[6] != null && arr[6] == arr[7] && arr[7] == arr[8]) {
            return arr[6];
        }

        if (arr[0] != null && arr[0] == arr[3] && arr[3] == arr[6]) {
            return arr[0];
        } else if (arr[1] != null && arr[1] == arr[4] && arr[4] == arr[7]) {
            return arr[1];
        } else if (arr[2] != null && arr[2] == arr[5] && arr[5] == arr[8]) {
            return arr[2];
        }

        if (arr[0] != null && arr[0] == arr[4] && arr[4] == arr[8]) {
            return arr[0];
        } else if (arr[2] != null && arr[2] == arr[4] && arr[4] == arr[6]) {
            return arr[2];
        }

        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 9; ++i) {
            sb.append((arr[i] == null) ? "_" : arr[i].toString());
            if ((i + 1) % 3 == 0) {
                sb.append("\n");
            } else {
                sb.append("|");
            }
        }
        return sb.toString();
    }
}
