package ru.tictactoe.model;

public enum TicTacToeSymbol {
    X, O
}