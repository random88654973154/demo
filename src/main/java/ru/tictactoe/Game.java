package ru.tictactoe;

import ru.tictactoe.model.TicTacToeModel;
import ru.tictactoe.model.TicTacToeSimpleModel;
import ru.tictactoe.model.TicTacToeSymbol;
import ru.tictactoe.service.TicTacToeAIPlayer;
import ru.tictactoe.service.TicTacToeInputBasedPlayer;
import ru.tictactoe.service.TicTacToePlayer;

public class Game {
    public static void main(String[] args) {
        TicTacToeModel field = new TicTacToeSimpleModel();

        TicTacToePlayer p1 = new TicTacToeInputBasedPlayer(TicTacToeSymbol.X);
        TicTacToePlayer p2 = new TicTacToeAIPlayer(TicTacToeSymbol.O);

        TicTacToePlayer current;
        TicTacToeSymbol winner = null;
        for (int i = 0; i < 9 && (winner = field.winner()) == null; ++i) {
            current = i % 2 == 0 ? p1 : p2;
            current.doMove(field);
            System.out.println(field.get());
        }

        if (winner == null) {
            System.out.println("Its a Draw!");
        } else {
            System.out.println("Player" + winner + " is Winner!");
        }
    }
}