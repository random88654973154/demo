package ru.tictactoe.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ru.tictactoe.model.TicTacToeModel;
import ru.tictactoe.model.TicTacToeSymbol;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TicTacToeInputBasedPlayerTest {

    @Mock
    private TicTacToeModel model;
    @Mock
    private TicTacToeInputProcessor input;

    private TicTacToeInputBasedPlayer player;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        player = new TicTacToeInputBasedPlayer(TicTacToeSymbol.X);
        player.setInput(input);
        when(model.set(1, 1, TicTacToeSymbol.X)).thenReturn(true);
    }

    @Test
    public void testDoMove() {
        when(input.getInt()).thenReturn(1);
        player.doMove(model);
        verify(model).set(1, 1, TicTacToeSymbol.X);
    }
}
