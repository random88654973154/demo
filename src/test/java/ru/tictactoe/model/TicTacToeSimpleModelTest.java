package ru.tictactoe.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TicTacToeSimpleModelTest {

    private static final String FIELD = "_|_|_\n" +
            "_|_|_\n" +
            "_|_|_\n";
    private static final String FIELD2 = "X|_|_\n" +
            "_|_|_\n" +
            "_|_|_\n";
    private static final String FIELD3 = "X|_|_\n" +
            "_|_|O\n" +
            "_|_|_\n";

    private TicTacToeSimpleModel model;

    @Before
    public void init() {
        model = new TicTacToeSimpleModel();
    }

    @Test
    public void testGet() {
        assertEquals(FIELD, model.get());
    }

    @Test
    public void testSet() {
        assertTrue(model.set(1, 1, TicTacToeSymbol.X));
        assertTrue(model.set(2, 2, TicTacToeSymbol.O));
    }

    @Test
    public void testSetFailed() {
        assertFalse(model.set(0, 0, TicTacToeSymbol.X));
        assertFalse(model.set(-3, -1, TicTacToeSymbol.O));
        model.set(1, 1, TicTacToeSymbol.X);
        assertFalse(model.set(1, 1, TicTacToeSymbol.O));
    }

    @Test
    public void testSetGet() {
        model.set(1, 1, TicTacToeSymbol.X);
        assertEquals(FIELD2, model.get());

        model.set(2, 3, TicTacToeSymbol.O);
        assertEquals(FIELD3, model.get());
    }

    @Test
    public void testNoWinner() {
        assertNull(model.winner());
        model.set(1, 1, TicTacToeSymbol.X);
        model.set(2, 2, TicTacToeSymbol.O);
        model.set(3, 3, TicTacToeSymbol.X);
        assertNull(model.winner());
    }

    @Test
    public void testWinnerHorizontal() {
        model.set(1, 1, TicTacToeSymbol.X);
        model.set(1, 2, TicTacToeSymbol.X);
        model.set(1, 3, TicTacToeSymbol.X);
        assertEquals(TicTacToeSymbol.X, model.winner());
    }

    @Test
    public void testWinnerVertical() {
        model.set(1, 2, TicTacToeSymbol.X);
        model.set(2, 2, TicTacToeSymbol.X);
        model.set(3, 2, TicTacToeSymbol.X);
        assertEquals(TicTacToeSymbol.X, model.winner());
    }

    @Test
    public void testWinnerDiagonal() {
        model.set(1, 1, TicTacToeSymbol.O);
        model.set(2, 2, TicTacToeSymbol.O);
        model.set(3, 3, TicTacToeSymbol.O);
        assertEquals(TicTacToeSymbol.O, model.winner());
    }
}